import './index.scss';

import React from 'react';
import ReactDom from 'react-dom';
import InfiniteGrid from 'react-infinite-grid';

import Product from './components/product';
import ProductService from './services/productService';

let productService = new ProductService();
// Create 100,000 Example items
let items = [];
for (let i = 0; i <= 1000; i++) {
    let product = productService.getProduct();
  items.push(<Product product={product}/>);
}

ReactDom.render(<InfiniteGrid itemClassName={"item"} entries={items} />, document.getElementById('content'));


