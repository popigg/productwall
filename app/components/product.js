import React from 'react';

class Product extends React.Component {


    showLikes() {

    }

    render() {
        return(
            <div>                
                <img src={this.props.product.image} />
                <div className='price-label-container'>
                    <div className='price-label'>                    
                        <span className='price-label-price'>{this.props.product.sellingPrice}</span>
                        <span className='price-label-currency'>{this.props.product.currency}</span>
                    </div>
                </div>

                <div className='likes'>
                    <div className='indicator'>
                        {this.props.product.likesCount}
                    </div>                    
                </div>


                <div className='description'>                    
                    <div className='description-product-title'>{this.props.product.title}</div>
                    <div className='description-author'>
                        <span className='description-author-join'>by</span>
                        <a href="#">{this.props.product.author}</a>
                    </div>
                </div>
            </div>
        );
    }
}

export default Product