import Faker from 'faker';

class ProductService {

	getProduct() {
		return {
			id: Faker.random.number( {max: 9999} ),
			type: 'Product',
			title: Faker.commerce.productName().toUpperCase(),
			author: Faker.company.bsNoun(),
			sellingPrice: Faker.commerce.price(),
			currency: '€',
			likesCount: Faker.random.number( {max: 99} ),
			image: Faker.image.imageUrl(400, 267) + '?rnd=' + Faker.random.number({max: 10000000})			
		}
	}

}

export default ProductService;